<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 20:37
 */

namespace App\Http\HttpResponse;

class ErrorResponse implements \JsonSerializable
{
    private $message;

    public static function create(string $message): ErrorResponse
    {
        $response = new static();
        $response->message = $message;

        return $response;
    }

    public function jsonSerialize()
    {
        return ['message' => $this->message, 'status' => Status::ERROR];
    }
}
