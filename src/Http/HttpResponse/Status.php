<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 20:38
 */

namespace App\Http\HttpResponse;

class Status
{
    const SUCCESS = 'success';
    const ERROR = 'error';
}
