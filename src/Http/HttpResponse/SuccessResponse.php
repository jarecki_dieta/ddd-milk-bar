<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 20:44
 */

namespace App\Http\HttpResponse;

class SuccessResponse
{
    private $message;

    public static function create(string $message = 'ok'): SuccessResponse
    {
        $response = new static();
        $response->message = $message;

        return $response;
    }

    public function jsonSerialize()
    {
        return ['message' => $this->message, 'status' => Status::SUCCESS];
    }
}
