<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 18:32
 */

namespace App\Http\Controller;

use App\Application\UseCase\Offer\GetOfferUseCase;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CategoryController
 * @package App\Http\Controller
 *
 * @Rest\RouteResource("offer")
 */
class OfferController extends FOSRestController
{

    /**
     * @var GetOfferUseCase
     */
    private $getOfferUseCase;

    public function __construct(GetOfferUseCase $getOfferUseCase)
    {
        $this->getOfferUseCase = $getOfferUseCase;
    }

    public function getOfferAction()
    {
        $offer = $this->getOfferUseCase->execute();
        return new JsonResponse($offer);
    }
}
