<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 18:32
 */

namespace App\Http\Controller;

use App\Http\HttpResponse\SuccessResponse;
use App\Application\Request\Category\DeleteCategoryRequest;
use App\Application\UseCase\Category\CreateCategoryUseCase;
use App\Application\UseCase\Category\DeleteCategoryUseCase;
use App\Application\UseCase\Category\RenameCategoryUseCase;
use App\Http\Form\Category\CategoryType;
use App\Http\Form\Category\RenameCategoryType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CategoryController
 * @package App\Http\Controller
 *
 * @Rest\RouteResource("categories")
 */
class CategoryController extends FOSRestController
{

    /**
     * @var CreateCategoryUseCase
     */
    private $createCategoryUseCase;
    /**
     * @var RenameCategoryUseCase
     */
    private $renameCategoryUseCase;
    /**
     * @var DeleteCategoryUseCase
     */
    private $deleteCategoryUseCase;

    public function __construct(
                                CreateCategoryUseCase $createCategoryUseCase,
                                RenameCategoryUseCase $renameCategoryUseCase,
                                DeleteCategoryUseCase $deleteCategoryUseCase
    ) {
        $this->createCategoryUseCase = $createCategoryUseCase;
        $this->renameCategoryUseCase = $renameCategoryUseCase;
        $this->deleteCategoryUseCase = $deleteCategoryUseCase;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Domain\Catalog\Category\CategoryAlreadyExistsException
     */
    public function postCreateAction(Request $request)
    {
        $form = $this->createForm(CategoryType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->handleView($this->view($form));
        }
        /**
         * @TODO:
         * Handle exceptions
         */
        $response = $this->createCategoryUseCase->execute($form->getData());

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @return View|\Symfony\Component\Form\FormInterface
     */
    public function putRenameAction(Request $request)
    {
        $form = $this->createForm(RenameCategoryType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        /**
         * @TODO:
         * Handle exceptions
         */
        $response = $this->renameCategoryUseCase->execute($form->getData());

        return new JsonResponse($response);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Domain\Catalog\Category\CategoryAssignedToProductsException
     */
    public function deleteRemoveAction(Request $request)
    {
        $useCaseRequest = new DeleteCategoryRequest($request->request->get('categoryId'));
        /**
         * @TODO:
         * Handle exceptions
         */
        $this->deleteCategoryUseCase->execute($useCaseRequest);
        return new JsonResponse(SuccessResponse::create());
    }
}
