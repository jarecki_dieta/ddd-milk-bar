<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 18:33
 */

namespace App\Http\Controller;

use App\Http\HttpResponse\SuccessResponse;
use App\Application\Request\Promotion\RemovePromotionRequest;
use App\Application\UseCase\Promotion\CreatePromotionUseCase;
use App\Application\UseCase\Promotion\EditPromotionUseCase;
use App\Application\UseCase\Promotion\RemovePromotionUseCase;
use App\Http\Form\Promotion\CreatePromotionType;
use App\Http\Form\Promotion\EditPromotionType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CategoryController
 * @package App\Http\Controller
 *
 * @Rest\RouteResource("products")
 */

class PromotionController extends FOSRestController
{


    /**
     * @var CreatePromotionUseCase
     */
    private $createPromotionUseCase;
    /**
     * @var EditPromotionUseCase
     */
    private $editPromotionUseCase;
    /**
     * @var RemovePromotionUseCase
     */
    private $removePromotionUseCase;

    public function __construct(
        CreatePromotionUseCase $createPromotionUseCase,
                                EditPromotionUseCase $editPromotionUseCase,
                                RemovePromotionUseCase $removePromotionUseCase
    ) {
        $this->createPromotionUseCase = $createPromotionUseCase;
        $this->editPromotionUseCase = $editPromotionUseCase;
        $this->removePromotionUseCase = $removePromotionUseCase;
    }

    public function postCreateAction(Request $request)
    {
        $form = $this->createForm(CreatePromotionType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->handleView($this->view($form));
        }
        /**
         * @TODO:
         * Handle exceptions
         */
        $response = $this->createPromotionUseCase->execute($form->getData());

        return new JsonResponse($response);
    }

    public function putEditAction(Request $request)
    {
        $form = $this->createForm(EditPromotionType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        /**
         * @TODO:
         * Handle exceptions
         */
        $response = $this->editPromotionUseCase->execute($form->getData());

        return new JsonResponse($response);
    }

    public function deleteRemoveAction(Request $request)
    {
        $useCaseRequest = new RemovePromotionRequest($request->request->get('promotionId'));
        /**
         * @TODO:
         * Handle exceptions
         */
        $this->removePromotionUseCase->execute($useCaseRequest);
        return new JsonResponse(SuccessResponse::create());
    }
}
