<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 18:33
 */

namespace App\Http\Controller;

use App\Http\HttpResponse\SuccessResponse;
use App\Application\Request\Product\RemoveProductRequest;
use App\Application\UseCase\Product\CreateProductUseCase;
use App\Application\UseCase\Product\EditProductUseCase;
use App\Application\UseCase\Product\RemoveProductUseCase;
use App\Http\Form\Product\CreateProductType;
use App\Http\Form\Product\EditProductType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CategoryController
 * @package App\Http\Controller
 *
 * @Rest\RouteResource("products")
 */

class ProductController extends FOSRestController
{
    /**
     * @var CreateProductUseCase
     */
    private $createProductUseCase;
    /**
     * @var EditProductUseCase
     */
    private $editProductUseCase;
    /**
     * @var RemoveProductUseCase
     */
    private $removeProductUseCase;

    public function __construct(
        CreateProductUseCase $createProductUseCase,
                                EditProductUseCase $editProductUseCase,
                                RemoveProductUseCase $removeProductUseCase
    ) {
        $this->createProductUseCase = $createProductUseCase;
        $this->editProductUseCase = $editProductUseCase;
        $this->removeProductUseCase = $removeProductUseCase;
    }

    public function postCreateAction(Request $request)
    {
        $form = $this->createForm(CreateProductType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $this->handleView($this->view($form));
        }
        /**
         * @TODO:
         * Handle exceptions
         */
        $response = $this->createProductUseCase->execute($form->getData());

        return new JsonResponse($response);
    }

    public function putEditAction(Request $request)
    {
        $form = $this->createForm(EditProductType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        /**
         * @TODO:
         * Handle exceptions
         */
        $response = $this->editProductUseCase->execute($form->getData());

        return new JsonResponse($response);
    }

    public function deleteRemoveAction(Request $request)
    {
        $useCaseRequest = new RemoveProductRequest($request->request->get('productId'));
        /**
         * @TODO:
         * Handle exceptions
         */
        $this->removeProductUseCase->execute($useCaseRequest);
        return new JsonResponse(SuccessResponse::create());
    }
}
