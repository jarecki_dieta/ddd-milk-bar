<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 19:00
 */

namespace App\Http\Form\Promotion;

use App\Application\Request\Product\CreateProductRequest;
use App\Domain\Catalog\Promotion\PromotionType;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreatePromotionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name'
            ])
            ->add('description', TextType::class, [
                'label' => 'Description'
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Promotion type',
                'choices' => [
                    PromotionType::TYPE_DISCOUNT => 'Discount',
                    PromotionType::TYPE_PRODUCT => 'Product'
                ]
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'Is active'
            ])
            ->add('requiredCategoryId', ChoiceType::class, [
                'label' => 'CategoryId',
                'choices' => [] /** @TODO: add choices */
            ])
            ->add('requiredProductId', ChoiceType::class, [
                'label' => 'CategoryId',
                'choices' => [] /** @TODO: add choices */
            ])
            ->add('hoursFrom', TextType::class, ['label' => 'Hour from'])
            ->add('hoursTo', TextType::class, ['label' => 'Hour to'])
            ->add('days', ChoiceType::class, [
                'label' => 'Days',
                'choices' => [
                    1 => 'Monday',
                    2 => 'Tuesday',
                    3 => 'Wednesday',
                    4 => 'Thursday',
                    5 => 'Friday',
                    6 => 'Saturday',
                    7 => 'Sunday',
                ]
            ])
            ->add('promotionProductId', ChoiceType::class, [
                'label' => 'Promotion product',
                'choices' => [] /** @TODO: add choices */
            ])
            ->add('discount', IntegerType::class, [
                'label' => 'Discount'
            ])

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CreateProductRequest::class,
            'csrf_protection' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'product';
    }
}
