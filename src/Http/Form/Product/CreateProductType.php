<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 19:00
 */

namespace App\Http\Form\Product;

use App\Application\Request\Product\CreateProductRequest;
use Doctrine\DBAL\Types\FloatType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name'
            ])
            ->add('description', TextType::class, [
                'label' => 'Description'
            ])
            ->add('price', FloatType::class, [
                'label' => 'Price'
            ])
            ->add('categoryId', ChoiceType::class, [
                'label' => 'CategoryId',
                'choices' => [] /** @TODO: add choices */
            ])

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CreateProductRequest::class,
            'csrf_protection' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'product';
    }
}
