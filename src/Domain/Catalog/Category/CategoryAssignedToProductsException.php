<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 10:19
 */

namespace App\Domain\Catalog\Category;

class CategoryAssignedToProductsException extends \Exception
{
}
