<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 13:24
 */

namespace App\Domain\Catalog\Category;

use App\Domain\Common\ValueObject\Name;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="App\Infrastructure\Persistence\Doctrine\CategoryRepository")
 */
class Category
{
    /**
     * @var CategoryId
     *
     * @ORM\Column(name="id", type="categoryId")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @ORM\Embedded(class="App\Domain\Common\ValueObject\Name", columnPrefix="name")
     */
    private $name;

    public function __construct(CategoryId $id, Name $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function id(): CategoryId
    {
        return $this->id;
    }

    public function name(): Name
    {
        return $this->name;
    }

    public function rename(Name $name): void
    {
        $this->name = $name;
    }
}
