<?php
/**
 * User: jszutkowski
 * Date: 07.04.18
 * Time: 14:34
 */

namespace App\Domain\Catalog\Category;

class CategoryAlreadyExistsException extends \Exception
{
}
