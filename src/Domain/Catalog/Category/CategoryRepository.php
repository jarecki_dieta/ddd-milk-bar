<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 10:22
 */

namespace App\Domain\Catalog\Category;

use App\Domain\Common\Repository\BaseRepositoryInterface;
use App\Domain\Common\ValueObject\Name;

interface CategoryRepository extends BaseRepositoryInterface
{
    public function nextId(): CategoryId;
    public function ofId(CategoryId $id): ?Category;
    public function checkIfExists(Name $name): bool;
}
