<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 11:30
 */

namespace App\Domain\Catalog\Category;

use App\Domain\Common\ValueObject\Name;

class CategoryFactory
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function create(string $name): Category
    {
        $uuid = $this->categoryRepository->nextId();
        return new Category($uuid, Name::fromString($name));
    }
}
