<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 10:16
 */

namespace App\Domain\Catalog\Category;

use App\Domain\Catalog\Product\ProductRepository;

class CategoryDeleteService
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
                                ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param Category $category
     * @throws CategoryAssignedToProductsException
     */
    public function delete(Category $category): void
    {
        if (!$this->canDeleteCategory($category)) {
            throw new CategoryAssignedToProductsException();
        }

        $this->categoryRepository->delete($category);
    }

    private function canDeleteCategory(Category $category): bool
    {
        $amountOfProducts = $this->productRepository->countByCategoryId($category->id());
        return $amountOfProducts == 0;
    }
}
