<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 10:22
 */

namespace App\Domain\Catalog\Product;

use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Common\Repository\BaseRepositoryInterface;

interface ProductRepository extends BaseRepositoryInterface
{
    public function nextId(): ProductId;
    public function ofId(ProductId $id): ?Product;
    public function countByCategoryId(CategoryId $id): int;
}
