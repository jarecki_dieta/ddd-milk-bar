<?php
/**
 * Created by PhpStorm.
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 10:20
 */

namespace App\Domain\Catalog\Product;

use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Common\ValueObject\Money;
use App\Domain\Common\ValueObject\Name;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="App\Infrastructure\Persistence\Doctrine\ProductRepository")
 */
class Product
{
    /**
     * @var ProductId
     *
     * @ORM\Column(name="id", type="productId")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @ORM\Embedded(class="App\Domain\Common\ValueObject\Name")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Embedded(class="App\Domain\Common\ValueObject\Money", columnPrefix="price_")
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(type="categoryId", nullable=false)
     */
    private $categoryId;

    public function __construct(ProductId $id,
                                Name $name,
                                string $description,
                                Money $price,
                                CategoryId $categoryId)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
        $this->categoryId = $categoryId;
    }

    public static function create(ProductId $id,
                                  string $name,
                                  string $description,
                                  float $price,
                                  string $currency,
                                  CategoryId $categoryId): Product
    {
        $name = Name::fromString($name);
        $productPrice = Money::create($price, $currency);

        return new Product($id, $name, $description, $productPrice, $categoryId);
    }

    public function id(): ProductId
    {
        return $this->id;
    }

    public function name(): Name
    {
        return $this->name;
    }

    public function price(): Money
    {
        return $this->price;
    }

    public function categoryId(): CategoryId
    {
        return $this->categoryId;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function rename(Name $name): void
    {
        $this->name = $name;
    }

    public function changeDescription(string $description)
    {
        $this->description = $description;
    }

    public function modifyPrice(Money $price)
    {
        $this->price = $price;
    }

    public function assignToCategory(CategoryId $categoryId)
    {
        if (!$this->categoryId->equals($categoryId)) {
            $this->categoryId = $categoryId;
        }
    }
}
