<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 10:22
 */

namespace App\Domain\Catalog\Product;

use App\Domain\AggregateRootId;

class ProductId extends AggregateRootId
{
}
