<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 18:32
 */

namespace App\Domain\Catalog\Product;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class ProductQuantity
{

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    public function __construct(int $quantity)
    {
        Assert::greaterThanEq($quantity, 1);
        $this->quantity = $quantity;
    }

    public static function create(int $quantity): ProductQuantity
    {
        return new static($quantity);
    }

    public function quantity(): int
    {
        $this->quantity;
    }
}
