<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 14:47
 */

namespace App\Domain\Catalog\Promotion;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class Hours implements \JsonSerializable
{

    /**
     * @ORM\Embedded(class="App\Domain\Catalog\Promotion\Hour", columnPrefix="hour_from_")
     */
    private $hourFrom;

    /**
     * @ORM\Embedded(class="App\Domain\Catalog\Promotion\Hour", columnPrefix="hour_to_")
     */
    private $hourTo;

    public function __construct(Hour $hourFrom, Hour $hourTo)
    {
        $this->validate($hourFrom, $hourTo);

        $this->hourFrom = $hourFrom;
        $this->hourTo = $hourTo;
    }

    public static function createEmpty(): Hours
    {
        return new static(new Hour(0, 0), new Hour(0, 0));
    }

    public static function create(Hour $hourFrom, Hour $hourTo): Hours
    {
        return new static($hourFrom, $hourTo);
    }

    private function validate(Hour $hourFrom, Hour $hourTo)
    {
        Assert::lessThanEq($hourFrom->hour(), $hourTo->hour(), 'Hour from must be less than or equal to date to');
        if ($hourFrom->hour() == $hourTo->hour()) {
            Assert::lessThanEq($hourFrom->minutes(), $hourTo->minutes(), 'Minutes from must be less than or equal to minutes to');
        }
    }

    public function isTimeBetweenHours(): bool
    {
        $currentHour = (int) date('G');
        $currentMinutes = (int) date('i');

        if ($currentHour < $this->hourFrom->hour() || $currentHour > $this->hourTo->hour()) {
            return false;
        }

        if ($currentHour == $this->hourFrom->hour() && $currentMinutes < $this->hourFrom->minutes()) {
            return false;
        }

        if ($currentHour == $this->hourTo->hour() && $currentMinutes > $this->hourTo->minutes()) {
            return false;
        }

        return true;
    }

    public function jsonSerialize()
    {
        return ['hourFrom' => $this->hourFrom, 'hourTo' => $this->hourTo];
    }
}
