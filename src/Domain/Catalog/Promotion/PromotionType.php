<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 14:40
 */

namespace App\Domain\Catalog\Promotion;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class PromotionType
{
    const TYPE_UNINITIALIZED = -1;
    const TYPE_PRODUCT = 1;
    const TYPE_DISCOUNT = 2;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    public function __construct(int $type = self::TYPE_UNINITIALIZED)
    {
        Assert::oneOf($type, [
            static::TYPE_UNINITIALIZED,
            static::TYPE_PRODUCT,
            static::TYPE_DISCOUNT
        ], 'Invalid promotion type');

        $this->type = $type;
    }

    public static function ofType(int $type): PromotionType
    {
        return new static($type);
    }

    public static function createUninitialized(): PromotionType
    {
        return new static(static::TYPE_UNINITIALIZED);
    }

    public static function createProduct(): PromotionType
    {
        return new static(static::TYPE_PRODUCT);
    }

    public static function createDiscount(): PromotionType
    {
        return new static(static::TYPE_DISCOUNT);
    }

    public function type(): int
    {
        return $this->type;
    }

    public function isUninitialized(): bool
    {
        return static::TYPE_UNINITIALIZED == $this->type;
    }

    public function isProductPromotion(): bool
    {
        return static::TYPE_PRODUCT == $this->type;
    }

    public function isDiscountPromotion(): bool
    {
        return static::TYPE_DISCOUNT == $this->type;
    }
}
