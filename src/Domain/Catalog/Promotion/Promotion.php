<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 14:25
 */

namespace App\Domain\Catalog\Promotion;

use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Catalog\Product\ProductId;
use App\Domain\Common\ValueObject\Name;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="promotion")
 * @ORM\Entity(repositoryClass="App\Infrastructure\Persistence\Doctrine\PromotionRepository")
 */
class Promotion
{
    /**
     * @var PromotionId
     *
     * @ORM\Column(name="id", type="promotionId")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @ORM\Embedded(class="App\Domain\Common\ValueObject\Name")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Embedded(class="App\Domain\Catalog\Promotion\PromotionType", columnPrefix="type_")
     */
    private $type;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isActive = true;

    /**
     * @var ProductId
     * @ORM\Column(type="productId", nullable=true)
     */
    private $requiredProductId;

    /**
     * @var CategoryId
     * @ORM\Column(type="categoryId", nullable=true)
     */
    private $requiredCategoryId;

    /**
     * @ORM\Embedded(class="App\Domain\Catalog\Promotion\RequiredQuantity", columnPrefix="quantity_")
     */
    private $requiredQuantity;

    /**
     * @ORM\Embedded(class="App\Domain\Catalog\Promotion\Hours", columnPrefix="hours_")
     */
    private $hours;

    /**
     * @ORM\Embedded(class="App\Domain\Catalog\Promotion\Days", columnPrefix="days_")
     */
    private $days;

    /**
     * @var ProductId
     * @ORM\Column(type="productId", nullable=true)
     */
    private $promotionProductId;

    /**
     * @ORM\Embedded(class="App\Domain\Catalog\Promotion\PercentageDiscount", columnPrefix="discount_")
     */
    private $discount;

    public function __construct(
        PromotionId $id,
                                Name $name,
                                string $description,
                                bool $isActive
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->isActive = $isActive;

        $this->type = PromotionType::createUninitialized();
        $this->days = Days::create();
        $this->hours = Hours::createEmpty();
        $this->requiredQuantity = RequiredQuantity::create();
    }

    public function id(): PromotionId
    {
        return $this->id;
    }

    public function name(): Name
    {
        return $this->name;
    }

    public function rename(Name $name): void
    {
        $this->name = $name;
    }

    public function description(): string
    {
        return $this->description;
    }

    public function changeDescription(string $description): void
    {
        $this->description = $description;
    }

    public function activate(): void
    {
        $this->isActive = true;
    }

    public function deactivate(): void
    {
        $this->isActive = false;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function productId(): ?ProductId
    {
        return $this->productId;
    }

    public function categoryId(): ?CategoryId
    {
        return $this->categoryId;
    }

    public function promotionProductId(): ?ProductId
    {
        return $this->promotionProductId;
    }

    public function discount(): ?PercentageDiscount
    {
        return $this->discount;
    }

    public function type(): PromotionType
    {
        return $this->type;
    }

    public function requiredProductQuantity(): RequiredQuantity
    {
        return $this->requiredQuantity;
    }

    public function days(): Days
    {
        return $this->days;
    }

    public function hours(): Hours
    {
        return $this->hours;
    }


    public function createProductPromotion(Days $days, Hours $hours, RequiredQuantity $requiredQuantity, ?ProductId $productId, ?CategoryId $categoryId, ProductId $promotionProduct)
    {
        $this->type = PromotionType::createProduct();
        $this->promotionProductId = $promotionProduct;
        $this->createPromotion($days, $hours, $requiredQuantity, $productId, $categoryId);
    }

    public function createDiscountPromotion(Days $days, Hours $hours, RequiredQuantity $requiredQuantity, ?ProductId $productId, ?CategoryId $categoryId, PercentageDiscount $discount)
    {
        $this->type = PromotionType::createDiscount();
        $this->discount = $discount;
        $this->createPromotion($days, $hours, $requiredQuantity, $productId, $categoryId);
    }

    private function createPromotion(Days $days, Hours $hours, RequiredQuantity $requiredQuantity, ?ProductId $productId, ?CategoryId $categoryId)
    {
        $this->days = $days;
        $this->hours = $hours;
        $this->requiredQuantity = $requiredQuantity;
        $this->requiredProductId = $productId;
        $this->requiredCategoryId = $categoryId;
    }
}
