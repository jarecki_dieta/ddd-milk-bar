<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 10:27
 */

namespace App\Domain\Catalog\Promotion;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class PercentageDiscount
{
    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    public function __construct(int $discount)
    {
        Assert::greaterThan($discount, 0, 'Discount should be greater than 0');
        Assert::lessThanEq($discount, 100, 'Discount should be equal or less than 100');

        $this->value = $discount;
    }

    public static function create(int $discount): PercentageDiscount
    {
        return new static($discount);
    }

    public function value(): int
    {
        return $this->value;
    }
}
