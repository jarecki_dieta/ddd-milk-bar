<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 10:22
 */

namespace App\Domain\Catalog\Promotion;

use App\Domain\Common\Repository\BaseRepositoryInterface;

interface PromotionRepository extends BaseRepositoryInterface
{
    public function nextId(): PromotionId;
    public function ofId(PromotionId $id): ?Promotion;
}
