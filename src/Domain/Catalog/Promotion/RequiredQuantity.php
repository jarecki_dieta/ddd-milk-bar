<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 14:46
 */

namespace App\Domain\Catalog\Promotion;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class RequiredQuantity
{
    const MIN_VALUE = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    public function __construct(int $value)
    {
        Assert::greaterThanEq($value, static::MIN_VALUE, sprintf('Value should be equal or greater than %d', [static::MIN_VALUE]));
        $this->value = $value;
    }

    public static function create(int $value = 1): RequiredQuantity
    {
        return new static($value);
    }

    public function value(): int
    {
        return $this->value;
    }
}
