<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 14:47
 */

namespace App\Domain\Catalog\Promotion;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class Days
{
    /**
     * @ORM\Column(type="array")
     */
    private $days = [];

    /**
     * Days constructor.
     * @param int[] $daysOfWeek
     */
    public function __construct(array $daysOfWeek = [])
    {
        $this->validateUniqueness($daysOfWeek);
        $this->validate($daysOfWeek);
        $this->days = $daysOfWeek;
    }

    public static function create(array $daysOfWeek = []): Days
    {
        return new static($daysOfWeek);
    }

    private function validate(array $daysOfWeek)
    {
        foreach ($daysOfWeek as $day) {
            Assert::integer($day);
            Assert::greaterThanEq($day, 1, 'Invalid day');
            Assert::lessThanEq($day, 7, 'Invalid day');
        }
    }

    private function validateUniqueness(array $daysOfWeek)
    {
        $unique = array_unique($daysOfWeek);
        Assert::count($unique, count($daysOfWeek));
    }

    public function hasCurrentDay(): bool
    {
        return in_array(date('N'), $this->days);
    }

    public function isEmpty(): bool
    {
        return empty($this->days);
    }

    public function days(): array
    {
        return $this->days;
    }
}
