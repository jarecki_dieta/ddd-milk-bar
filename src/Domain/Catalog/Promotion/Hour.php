<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 15:43
 */

namespace App\Domain\Catalog\Promotion;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class Hour implements \JsonSerializable
{

    /**
     * @ORM\Column(type="integer")
     */
    private $hour;

    /**
     * @ORM\Column(type="integer")
     */
    private $minutes;

    public function __construct(int $hour, int $minutes)
    {
        Assert::greaterThanEq($hour, 0, 'Invalid hour');
        Assert::lessThanEq($hour, 23, 'Invalid hour');

        Assert::greaterThanEq($hour, 0, 'Invalid minutes');
        Assert::lessThanEq($hour, 59, 'Invalid minutes');

        $this->hour = $hour;
        $this->minutes = $minutes;
    }

    public static function fromString(string $time): Hour
    {
        $timeParts = explode(':', $time);
        Assert::count($timeParts, 2);

        return new static((int) $timeParts[0], (int) $timeParts[1]);
    }

    public function hour(): int
    {
        return $this->hour;
    }

    public function hourWithZero(): string
    {
        if ($this->hour < 10) {
            return '0' . $this->hour;
        }

        return $this->hour . '';
    }

    public function minutes(): int
    {
        return $this->minutes;
    }

    public function minutesWithZero(): string
    {
        if ($this->minutes < 10) {
            return '0' . $this->minutes;
        }

        return $this->minutes . '';
    }

    public function jsonSerialize()
    {
        return ['hour' => $this->hour, 'minutes' => $this->minutes];
    }
}
