<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 11:35
 */

namespace App\Domain;

use App\Domain\Common\Exception\InvalidUUIDException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

abstract class AggregateRootId
{
    /**
     * @var UuidInterface
     */
    private $id;

    public function __construct(?string $id)
    {
        try {
            $this->id = $id ? Uuid::fromString($id) : Uuid::uuid4();
        } catch (\InvalidArgumentException $e) {
            throw new InvalidUUIDException();
        }
    }

    public static function create()
    {
        return new static(null);
    }

    public static function fromString(string $id)
    {
        return new static($id);
    }

    public function id()
    {
        return $this->id;
    }

    public function equals(AggregateRootId $identifier): bool
    {
        return $this->id->equals($identifier->id()) && get_class($this) == get_class($identifier);
    }

    public function __toString()
    {
        return (string) $this->id;
    }
}
