<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 15:36
 */

namespace App\Domain\Tests;

use App\Domain\Catalog\Product\ProductId;
use App\Domain\Common\Exception\InvalidUUIDException;
use PHPUnit\Framework\AssertionFailedError;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class ProductIdTest extends TestCase
{
    public function testGenerateValid()
    {
        $uuid = (string) Uuid::uuid4();
        $productId = ProductId::fromString($uuid);

        $this->assertEquals((string)$productId->id(), $uuid);
        $this->assertEquals((string)$productId->__toString(), $uuid);
        $this->assertTrue($productId->equals(new ProductId($uuid)));


        $emptyUuid = '';
        $newProductId = ProductId::fromString($emptyUuid);
        $this->assertNotEmpty($newProductId->id());
    }

    public function testByConstructor()
    {
        $uuid = (string) Uuid::uuid4();
        $productId = new ProductId($uuid);

        $this->assertEquals((string)$productId->id(), $uuid);
        $this->assertTrue($productId->equals(new ProductId($uuid)));

        $newProductId = new ProductId('');
        $this->assertNotEmpty($newProductId->id());
    }

    public function testGenerateInvalid()
    {
        $strings = [
            substr(Uuid::uuid4(), 0, -1),
            'sada',
            'asd-asd-das-asd-sd',
            '1',
            '3546-56-6456-456-456'
        ];

        foreach ($strings as $string) {
            $this->createInstance($string);
            $this->createInstanceByConstructor($string);
        }
    }

    public function testGenerateValidWithError()
    {
        $this->expectException(AssertionFailedError::class);
        $this->createInstance(Uuid::uuid4());
    }

    public function testGenerateValidByConstructorWithError()
    {
        $this->expectException(AssertionFailedError::class);
        $this->createInstanceByConstructor(Uuid::uuid4());
    }

    private function createInstance(string $string)
    {
        try {
            ProductId::fromString($string);
            $this->fail('Exception should be thrown');
        } catch (\Exception $e) {
            $this->assertEquals(InvalidUUIDException::class, get_class($e));
        }
    }

    private function createInstanceByConstructor(string $string)
    {
        try {
            new ProductId($string);
            $this->fail('Exception should be thrown');
        } catch (\Exception $e) {
            $this->assertEquals(InvalidUUIDException::class, get_class($e));
        }
    }
}
