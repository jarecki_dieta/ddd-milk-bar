<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 13:56
 */

namespace App\Domain\Tests;

use App\Domain\Common\ValueObject\Name;
use PHPUnit\Framework\TestCase;

class ProductNameTest extends TestCase
{
    public function testCreationFromTooShortStrings()
    {
        for ($i = 0; $i < Name::MIN_LENGTH; $i++) {
            try {
                new Name($this->generateString($i));
                $this->fail();
            } catch (\Exception $e) {
                $this->assertEquals(\InvalidArgumentException::class, get_class($e));
            }
        }
    }
    public function testCreationFromNormalLengthStrings()
    {
        for ($i = Name::MIN_LENGTH; $i < Name::MIN_LENGTH + 2; $i++) {
            $string = $this->generateString($i);
            $productName = new Name($string);
            $this->assertEquals($string, $productName->name());
        }
    }

    private function generateString($length)
    {
        $string = 'abcdefghijklmnoprstuwys1234567890';
        return substr(str_shuffle($string), 0, $length);
    }
}
