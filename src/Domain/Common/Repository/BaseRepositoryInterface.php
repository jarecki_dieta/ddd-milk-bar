<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 10:20
 */

namespace App\Domain\Common\Repository;

interface BaseRepositoryInterface
{
    public function findAll();
    public function save($object);
    public function delete($object);
}
