<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 17:16
 */

namespace App\Domain\Common\ValueObject;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class Money
{
    const CURRENCY_PLN = 'PLN';

    /**
     * @ORM\Column(type="float")
     */
    private $amount;

    /**
     * @ORM\Column(type="string")
     */
    private $currency;

    public function __construct(float $amount, string $currency = self::CURRENCY_PLN)
    {
        Assert::greaterThanEq($amount, 0, 'The amount should be equal or greater than 0');

        $this->amount = $amount;
        $this->currency = $currency;
    }

    public static function create(float $amount, string $currency = self::CURRENCY_PLN): Money
    {
        return new Money($amount, $currency);
    }

    public function amount(): float
    {
        return $this->amount;
    }

    public function currency(): string
    {
        return $this->currency;
    }
}
