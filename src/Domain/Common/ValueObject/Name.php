<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 13:04
 */

namespace App\Domain\Common\ValueObject;

use Webmozart\Assert\Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class Name
{
    const MIN_LENGTH = 3;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    public function __construct(string $name)
    {
        Assert::minLength($name, static::MIN_LENGTH, sprintf('Name should contain at least %d characters', [static::MIN_LENGTH]));
        $this->name = $name;
    }

    public static function fromString(string $name): Name
    {
        return new static($name);
    }

    public function name(): string
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }
}
