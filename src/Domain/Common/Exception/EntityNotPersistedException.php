<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 20:55
 */

namespace App\Domain\Common\Exception;

class EntityNotPersistedException extends \Exception
{
}
