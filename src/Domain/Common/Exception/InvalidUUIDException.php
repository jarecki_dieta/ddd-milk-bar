<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 12:04
 */

namespace App\Domain\Common\Exception;

class InvalidUUIDException extends \InvalidArgumentException
{


    /**
     * InvalidUUIDException constructor.
     */
    public function __construct()
    {
        parent::__construct('aggregate_root.exception.invalid_uuid', 400);
    }
}
