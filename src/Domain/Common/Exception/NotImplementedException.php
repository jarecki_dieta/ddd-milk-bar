<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 12:50
 */

namespace App\Domain\Common\Exception;

class NotImplementedException extends \Exception
{
}
