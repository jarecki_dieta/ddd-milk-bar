<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 20:38
 */

namespace App\Domain\Common\Exception;

class ResourceNotFoundException extends \Exception
{
}
