<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 14:25
 */

namespace App\Infrastructure\Persistence\Doctrine;

use App\Domain\Common\Exception\ResourceNotFoundException;
use Doctrine\ORM\EntityRepository;

abstract class AbstractRepository extends EntityRepository
{
    /**
     * @param object $object
     */
    public function save($object)
    {
        $this->_em->persist($object);
        $this->_em->flush($object);
    }

    /**
     * @param object $object
     */
    public function delete($object)
    {
        $this->_em->remove($object);
        $this->_em->flush($object);
    }

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $entity = parent::find($id, $lockMode, $lockVersion);

        if (!$entity) {
            throw new ResourceNotFoundException();
        }

        return $entity;
    }
}
