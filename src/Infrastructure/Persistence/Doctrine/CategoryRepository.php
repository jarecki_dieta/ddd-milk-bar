<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 14:24
 */

namespace App\Infrastructure\Persistence\Doctrine;

use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Catalog\Category\Category;
use App\Domain\Catalog\Category\CategoryRepository as CategoryRepositoryInterface;
use App\Domain\Common\ValueObject\Name;

class CategoryRepository extends AbstractRepository implements CategoryRepositoryInterface
{
    public function nextId(): CategoryId
    {
        return CategoryId::create();
    }

    public function ofId(CategoryId $id): ?Category
    {
        return $this->find($id);
    }

    public function checkIfExists(Name $name): bool
    {
        $category = $this->findOneBy(['name.name' => $name->name()]);
        return !!$category;
    }
}
