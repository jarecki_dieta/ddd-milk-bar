<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 14:24
 */

namespace App\Infrastructure\Persistence\Doctrine;

use App\Domain\Catalog\Promotion\Promotion;
use App\Domain\Catalog\Promotion\PromotionId;
use App\Domain\Catalog\Promotion\PromotionRepository as PromotionRepositoryInterface;

class PromotionRepository extends AbstractRepository implements PromotionRepositoryInterface
{
    public function nextId(): PromotionId
    {
        return PromotionId::create();
    }

    public function ofId(PromotionId $id): ?Promotion
    {
        return $this->find($id);
    }
}
