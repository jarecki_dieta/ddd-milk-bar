<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 14:24
 */

namespace App\Infrastructure\Persistence\Doctrine;

use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Catalog\Product\Product;
use App\Domain\Catalog\Product\ProductId;
use App\Domain\Catalog\Product\ProductRepository as ProductRepositoryInterface;

class ProductRepository extends AbstractRepository implements ProductRepositoryInterface
{
    public function nextId(): ProductId
    {
        return ProductId::create();
    }

    public function ofId(ProductId $id): ?Product
    {
        return $this->find($id);
    }

    public function countByCategoryId(CategoryId $id): int
    {
        return $this->count(['categoryId' => $id]);
    }
}
