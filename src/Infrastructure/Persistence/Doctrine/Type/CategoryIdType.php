<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 11:27
 */

namespace App\Infrastructure\Persistence\Doctrine\Type;

use App\Domain\Catalog\Category\CategoryId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Ramsey\Uuid\Doctrine\UuidType;

class CategoryIdType extends UuidType
{
    const NAME = 'categoryId';

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        return CategoryId::fromString($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function getName()
    {
        return self::NAME;
    }
}
