<?php
/**
 * User: jszutkowski
 * Date: 03.04.18
 * Time: 11:27
 */

namespace App\Infrastructure\Persistence\Doctrine\Type;

use App\Domain\Catalog\Product\ProductId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Ramsey\Uuid\Doctrine\UuidType;

class ProductIdType extends UuidType
{
    const NAME = 'productId';

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return $value;
        }

        return ProductId::fromString($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function getName()
    {
        return self::NAME;
    }
}
