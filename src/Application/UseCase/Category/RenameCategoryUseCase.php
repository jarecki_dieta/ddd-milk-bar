<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 20:17
 */

namespace App\Application\UseCase\Category;

use App\Application\Request\Category\RenameCategoryRequest;
use App\Application\Response\CategoryResponse;
use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Catalog\Category\CategoryRepository;
use App\Domain\Common\ValueObject\Name;

class RenameCategoryUseCase
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function execute(RenameCategoryRequest $request): CategoryResponse
    {
        $category = $this->categoryRepository->ofId(CategoryId::fromString($request->getCategoryId()));
        $category->rename(Name::fromString($request->getName()));
        $this->categoryRepository->save($category);

        return CategoryResponse::fromCategory($category);
    }
}
