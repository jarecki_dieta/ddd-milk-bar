<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 20:18
 */

namespace App\Application\UseCase\Category;

use App\Application\Request\Category\DeleteCategoryRequest;
use App\Domain\Catalog\Category\CategoryAssignedToProductsException;
use App\Domain\Catalog\Category\CategoryDeleteService;
use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Catalog\Category\CategoryRepository;

class DeleteCategoryUseCase
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var CategoryDeleteService
     */
    private $deleteService;

    public function __construct(
        CategoryRepository $categoryRepository,
                                CategoryDeleteService $deleteService
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->deleteService = $deleteService;
    }

    /**
     * @param DeleteCategoryRequest $request
     * @throws CategoryAssignedToProductsException
     */
    public function execute(DeleteCategoryRequest $request): void
    {
        $category = $this->categoryRepository->ofId(new CategoryId($request->getCategoryId()));
        $this->deleteService->delete($category);
    }
}
