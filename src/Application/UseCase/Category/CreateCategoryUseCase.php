<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 20:16
 */

namespace App\Application\UseCase\Category;

use App\Application\Request\Category\CreateCategoryRequest;
use App\Application\Response\CategoryResponse;
use App\Application\UseCase\AbstractUseCase;
use App\Domain\Catalog\Category\CategoryAlreadyExistsException;
use App\Domain\Catalog\Category\CategoryFactory;
use App\Domain\Catalog\Category\CategoryRepository;
use App\Domain\Common\ValueObject\Name;

class CreateCategoryUseCase extends AbstractUseCase
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    public function __construct(
        CategoryRepository $categoryRepository,
                                CategoryFactory $categoryFactory
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * @param CreateCategoryRequest $request
     * @return CategoryResponse
     * @throws CategoryAlreadyExistsException
     */
    public function execute(CreateCategoryRequest $request): CategoryResponse
    {
        $doesExist = $this->categoryRepository->checkIfExists(Name::fromString($request->getName()));

        if ($doesExist) {
            throw new CategoryAlreadyExistsException();
        }

        $category = $this->categoryFactory->create($request->getName());
        $this->categoryRepository->save($category);

        return CategoryResponse::fromCategory($category);
    }
}
