<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 10:37
 */

namespace App\Application\UseCase\Promotion;

use App\Application\Request\Promotion\RemovePromotionRequest;
use App\Domain\Catalog\Promotion\PromotionId;
use App\Domain\Catalog\Promotion\PromotionRepository;

class RemovePromotionUseCase
{
    /**
     * @var PromotionRepository
     */
    private $promotionRepository;

    public function __construct(PromotionRepository $promotionRepository)
    {
        $this->promotionRepository = $promotionRepository;
    }

    public function execute(RemovePromotionRequest $request)
    {
        $promotion = $this->promotionRepository->ofId(PromotionId::fromString($request->getPromotionId()));
        $this->promotionRepository->delete($promotion);
    }
}
