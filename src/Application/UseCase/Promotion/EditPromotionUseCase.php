<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 11:31
 */

namespace App\Application\UseCase\Promotion;

use App\Application\Request\Promotion\EditPromotionRequest;
use App\Application\Response\PromotionResponse;
use App\Domain\Catalog\Promotion\PromotionId;
use App\Domain\Common\ValueObject\Name;

class EditPromotionUseCase extends BasePromotionModificationUseCase
{
    public function execute(EditPromotionRequest $request): PromotionResponse
    {
        $promotion = $this->promotionRepository->ofId(PromotionId::fromString($request->getId()));
        $promotion->rename(Name::fromString($request->getName()));
        $promotion->changeDescription($request->getDescription());
        $request->isActive() ? $promotion->activate() : $promotion->deactivate();
        $this->createFromData($promotion, $request);

        return PromotionResponse::fromPromotion($promotion);
    }
}
