<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 11:31
 */

namespace App\Application\UseCase\Promotion;

use App\Application\Request\Promotion\CreatePromotionRequest;
use App\Application\Response\PromotionResponse;
use App\Domain\Catalog\Promotion\Promotion;
use App\Domain\Common\ValueObject\Name;

class CreatePromotionUseCase extends BasePromotionModificationUseCase
{
    public function execute(CreatePromotionRequest $request):PromotionResponse
    {
        $id = $this->promotionRepository->nextId();
        $promotion = new Promotion($id, Name::fromString($request->getName()), $request->getDescription(), $request->isActive());
        $this->createFromData($promotion, $request);

        return PromotionResponse::fromPromotion($promotion);
    }
}
