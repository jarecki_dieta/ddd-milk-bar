<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 12:22
 */

namespace App\Application\UseCase\Promotion;

use App\Application\Request\Product\CreateProductRequest;
use App\Application\Request\Product\EditProductRequest;
use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Catalog\Category\CategoryRepository;
use App\Domain\Catalog\Product\ProductId;
use App\Domain\Catalog\Product\ProductRepository;
use App\Domain\Catalog\Promotion\Days;
use App\Domain\Catalog\Promotion\Hour;
use App\Domain\Catalog\Promotion\Hours;
use App\Domain\Catalog\Promotion\PercentageDiscount;
use App\Domain\Catalog\Promotion\Promotion;
use App\Domain\Catalog\Promotion\PromotionRepository;
use App\Domain\Catalog\Promotion\PromotionType;
use App\Domain\Catalog\Promotion\RequiredQuantity;

abstract class BasePromotionModificationUseCase
{
    /**
     * @var PromotionRepository
     */
    protected $promotionRepository;
    /**
     * @var ProductRepository
     */
    protected $productRepository;
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    public function __construct(PromotionRepository $promotionRepository, ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->promotionRepository = $promotionRepository;
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param Promotion $promotion
     * @param CreateProductRequest|EditProductRequest $request
     */
    protected function createFromData(Promotion $promotion, $request)
    {
        $promotionType = PromotionType::ofType($request->getType());

        $hours = $this->getHours($request->getHourFrom(), $request->getHourTo());
        $days = Days::create($request->getDays());
        $requiredQuantity = RequiredQuantity::create($request->getRequiredQuantity());
        $productId = $this->getProductId($request->getRequiredProductId());
        $categoryId = $this->getCategoryId($request->getRequiredCategoryId());


        if ($promotionType->isDiscountPromotion()) {
            $promotion->createDiscountPromotion(
                $days,
                $hours,
                $requiredQuantity,
                $productId,
                $categoryId,
                PercentageDiscount::create($request->getDiscount())
            );
        } elseif ($promotionType->isProductPromotion()) {
            $promotion->createProductPromotion(
                $days,
                $hours,
                $requiredQuantity,
                $productId,
                $categoryId,
                $this->getProductId($request->getPromotionProductId())
            );
        }

        $this->productRepository->save($promotion);
    }

    protected function getHours(string $hourFrom, string $hourTo): Hours
    {
        return Hours::create(Hour::fromString($hourFrom), Hour::fromString($hourTo));
    }

    protected function getProductId($productId): ?ProductId
    {
        if (!$productId) {
            return null;
        }

        $product = $this->productRepository->ofId(ProductId::fromString($productId));
        return $product->id();
    }

    protected function getCategoryId($categoryId): ?CategoryId
    {
        if (!$categoryId) {
            return null;
        }

        $category = $this->categoryRepository->ofId(CategoryId::fromString($categoryId));
        return $category->id();
    }
}
