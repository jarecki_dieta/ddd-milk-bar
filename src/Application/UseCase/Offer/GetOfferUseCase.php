<?php
/**
 * User: jszutkowski
 * Date: 07.04.18
 * Time: 15:39
 */

namespace App\Application\UseCase\Offer;

use App\Application\Response\OfferResponse;
use App\Domain\Catalog\Category\CategoryRepository;
use App\Domain\Catalog\Product\ProductRepository;
use App\Domain\Catalog\Promotion\PromotionRepository;

class GetOfferUseCase
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var PromotionRepository
     */
    private $promotionRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
                                ProductRepository $productRepository,
                                PromotionRepository $promotionRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->promotionRepository = $promotionRepository;
    }

    public function execute(): OfferResponse
    {
        $categories = $this->getCategories();
        $products = $this->getProducts();
        $promotions = $this->getPromotions();

        return new OfferResponse($categories, $products, $promotions);
    }

    private function getCategories(): array
    {
        return $this->categoryRepository->findAll();
    }

    private function getProducts(): array
    {
        return $this->productRepository->findAll();
    }

    private function getPromotions(): array
    {
        return $this->promotionRepository->findAll();
    }
}
