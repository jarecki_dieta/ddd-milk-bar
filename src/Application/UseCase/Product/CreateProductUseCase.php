<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 13:07
 */

namespace App\Application\UseCase\Product;

use App\Application\Request\Product\CreateProductRequest;
use App\Application\Response\ProductResponse;
use App\Domain\Catalog\Product\Product;

class CreateProductUseCase extends AbstractModifyProductUseCase
{
    public function execute(CreateProductRequest $request): ProductResponse
    {
        $product = $this->createProduct($request);
        $this->productRepository->save($product);

        return ProductResponse::fromProduct($product);
    }

    private function createProduct(CreateProductRequest $request): Product
    {
        $productId = $this->productRepository->nextId();
        $categoryId = $this->getCategoryId($request->getCategoryId());

        return Product::create(
            $productId,
            $request->getName(),
            $request->getDescription(),
            $request->getPrice(),
            $request->getCurrency(),
            $categoryId
        );
    }
}
