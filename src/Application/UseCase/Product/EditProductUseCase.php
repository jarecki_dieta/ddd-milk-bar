<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 13:07
 */

namespace App\Application\UseCase\Product;

use App\Application\Request\Product\EditProductRequest;
use App\Application\Response\ProductResponse;
use App\Domain\Catalog\Product\Product;
use App\Domain\Catalog\Product\ProductId;
use App\Domain\Common\ValueObject\Money;
use App\Domain\Common\ValueObject\Name;

class EditProductUseCase extends AbstractModifyProductUseCase
{
    public function execute(EditProductRequest $request): ProductResponse
    {
        $product = $this->productRepository->ofId(ProductId::fromString($request->getId()));
        $this->modifyProduct($product, $request);
        $this->productRepository->save($product);

        return ProductResponse::fromProduct($product);
    }

    /**
     * @param Product $product
     * @param EditProductRequest $request
     * @return Product
     */
    private function modifyProduct(Product $product, EditProductRequest $request): Product
    {
        $categoryId = $this->getCategoryId($request->getCategoryId());

        $product->rename(Name::fromString($request->getName()));
        $product->changeDescription($request->getDescription());
        $product->modifyPrice(Money::create($request->getPrice(), $request->getCurrency()));
        $product->assignToCategory($categoryId);
    }
}
