<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 13:59
 */

namespace App\Application\UseCase\Product;

use App\Domain\Catalog\Category\CategoryId;
use App\Domain\Catalog\Category\CategoryRepository;
use App\Domain\Catalog\Product\ProductRepository;

abstract class AbstractModifyProductUseCase
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;


    public function __construct(ProductRepository $productRepository,
                                CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    protected function getCategoryId(string $categoryId): CategoryId
    {
        $category = $this->categoryRepository->ofId(CategoryId::fromString($categoryId));
        return $category->id();
    }
}
