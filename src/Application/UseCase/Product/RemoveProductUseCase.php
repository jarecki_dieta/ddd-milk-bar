<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 10:37
 */

namespace App\Application\UseCase\Product;

use App\Application\Request\Product\RemoveProductRequest;
use App\Domain\Catalog\Product\ProductId;
use App\Domain\Catalog\Product\ProductRepository;

class RemoveProductUseCase
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(RemoveProductRequest $request)
    {
        $product = $this->productRepository->ofId(ProductId::fromString($request->getProductId()));

        $this->productRepository->delete($product);
    }
}
