<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 20:19
 */

namespace App\Application\Request\Category;

class RenameCategoryRequest
{
    private $categoryId;
    private $name;

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return (string)$this->categoryId;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId($categoryId): void
    {
        $this->categoryId = (string)$categoryId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return (string)$this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = (string)$name;
    }
}
