<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 20:19
 */

namespace App\Application\Request\Category;

class CreateCategoryRequest
{
    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return (string)$this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = (string) $name;
    }
}
