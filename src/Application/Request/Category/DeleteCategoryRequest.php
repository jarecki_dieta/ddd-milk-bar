<?php
/**
 * User: jszutkowski
 * Date: 04.04.18
 * Time: 20:24
 */

namespace App\Application\Request\Category;

class DeleteCategoryRequest
{
    private $categoryId;

    public function __construct(?string $uuid)
    {
        $this->categoryId = $uuid;
    }

    public function getCategoryId(): ?string
    {
        return (string) $this->categoryId;
    }
}
