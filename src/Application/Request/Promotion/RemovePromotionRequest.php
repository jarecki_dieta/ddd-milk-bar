<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 10:37
 */

namespace App\Application\Request\Promotion;

class RemovePromotionRequest
{
    private $promotionId;

    public function __construct(string $promotionId)
    {
        $this->promotionId = $promotionId;
    }

    public function getPromotionId(): string
    {
        return $this->promotionId;
    }
}
