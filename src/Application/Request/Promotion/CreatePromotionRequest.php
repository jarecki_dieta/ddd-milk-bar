<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 11:21
 */

namespace App\Application\Request\Promotion;

class CreatePromotionRequest
{
    private $name;
    private $description;
    private $type;
    private $isActive;

    private $requiredProductId;
    private $requiredCategoryId;
    private $requiredQuantity;
    private $hourFrom;
    private $hourTo;
    private $days;
    private $promotionProductId;
    private $discount;


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getRequiredProductId()
    {
        return $this->requiredProductId;
    }

    /**
     * @param mixed $requiredProductId
     */
    public function setRequiredProductId($requiredProductId): void
    {
        $this->requiredProductId = $requiredProductId;
    }

    /**
     * @return mixed
     */
    public function getRequiredCategoryId()
    {
        return $this->requiredCategoryId;
    }

    /**
     * @param mixed $requiredCategoryId
     */
    public function setRequiredCategoryId($requiredCategoryId): void
    {
        $this->requiredCategoryId = $requiredCategoryId;
    }

    /**
     * @return mixed
     */
    public function getRequiredQuantity()
    {
        return $this->requiredQuantity;
    }

    /**
     * @param mixed $requiredQuantity
     */
    public function setRequiredQuantity($requiredQuantity): void
    {
        $this->requiredQuantity = $requiredQuantity;
    }

    /**
     * @return string
     */
    public function getHourFrom(): string
    {
        return $this->hourFrom;
    }

    /**
     * @param string $hourFrom
     */
    public function setHourFrom(string $hourFrom): void
    {
        $this->hourFrom = $hourFrom;
    }

    /**
     * @return string
     */
    public function getHourTo(): string
    {
        return $this->hourTo;
    }

    /**
     * @param string $hourTo
     */
    public function setHourTo(string $hourTo): void
    {
        $this->hourTo = $hourTo;
    }

    /**
     * @return array
     */
    public function getDays(): array
    {
        return $this->days;
    }

    /**
     * @param array $days
     */
    public function setDays(array $days): void
    {
        $this->days = $days;
    }

    /**
     * @return mixed
     */
    public function getPromotionProductId()
    {
        return $this->promotionProductId;
    }

    /**
     * @param mixed $promotionProductId
     */
    public function setPromotionProductId($promotionProductId): void
    {
        $this->promotionProductId = $promotionProductId;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount): void
    {
        $this->discount = $discount;
    }
}
