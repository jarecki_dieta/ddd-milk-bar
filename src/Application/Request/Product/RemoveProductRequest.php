<?php
/**
 * User: jszutkowski
 * Date: 05.04.18
 * Time: 12:28
 */

namespace App\Application\Request\Product;

class RemoveProductRequest
{
    private $productId;

    public function __construct(string $productId)
    {
        $this->productId = $productId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }
}
