<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 20:00
 */

namespace App\Application\Response;

use App\Domain\Catalog\Category\Category;

class CategoryResponse implements \JsonSerializable
{
    private $id;
    private $name;

    public static function fromCategory(Category $category): CategoryResponse
    {
        $object = new static();
        $object->id = (string)$category->id();
        $object->name = $category->name()->name();

        return $object;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
