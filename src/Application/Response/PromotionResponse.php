<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 20:00
 */

namespace App\Application\Response;

use App\Domain\Catalog\Promotion\Promotion;

class PromotionResponse implements \JsonSerializable
{
    private $id;
    private $name;
    protected $description;
    protected $type;
    protected $isActive;
    protected $requiredQuantity;
    protected $requiredProductId;
    protected $requiredCategoryId;
    protected $days;
    protected $hours;

    public static function fromPromotion(Promotion $promotion): PromotionResponse
    {
        $object = new static();
        $object->id = (string)$promotion->id();
        $object->name = $promotion->name()->name();
        $object->description = $promotion->description();
        $object->type = $promotion->type()->type();
        $object->isActive = $promotion->isActive();
        $object->requiredQuantity = $promotion->requiredProductQuantity()->value();
        $object->requiredProductId = $promotion->productId()->id();
        $object->requiredCategoryId = $promotion->categoryId()->id();
        $object->days = $promotion->days()->days();
        $object->hours = $promotion->hours();

        return $object;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
