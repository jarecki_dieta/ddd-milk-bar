<?php
/**
 * User: jszutkowski
 * Date: 07.04.18
 * Time: 16:00
 */

namespace App\Application\Response;

use Webmozart\Assert\Assert;

class OfferResponse implements \JsonSerializable
{
    private $categories;
    private $products;
    private $promotions;

    /**
     * OfferResponse constructor.
     * @param $categories
     * @param $products
     * @param $promotions
     */
    public function __construct(array $categories, array $products, array $promotions)
    {
        $this->categories = $categories;
        $this->products = $products;
        $this->promotions = $promotions;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
