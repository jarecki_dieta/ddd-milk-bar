<?php
/**
 * User: jszutkowski
 * Date: 06.04.18
 * Time: 20:00
 */

namespace App\Application\Response;

use App\Domain\Catalog\Product\Product;

class ProductResponse implements \JsonSerializable
{
    private $id;
    private $name;
    private $description;
    private $categoryId;
    private $currency;
    private $price;

    public static function fromProduct(Product $product): ProductResponse
    {
        $object = new static();
        $object->id = (string)$product->id();
        $object->name = $product->name()->name();
        $object->description = $product->description();
        $object->categoryId = $product->categoryId();
        $object->price = $product->price()->amount();
        $object->currency = $product->price()->currency();

        return $object;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
